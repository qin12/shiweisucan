package com.shiweisucan.dao;

import com.shiweisucan.model.Shop;

import java.util.List;

public interface ShopDao {
    public Shop findBySid(int sid);
    public Shop findByUid(int uid);
    public List<Shop> showAllShops();
    public boolean addShop(Shop shop);
    public boolean modifyShop(Shop shop);
}
