package com.shiweisucan.dao;

import com.shiweisucan.model.Order;

import java.util.List;

public interface OrderDao1 {
    public List<Order> showAllOrders();
    public Boolean addOrder(Order order);
}
