package com.shiweisucan.dao;

import com.shiweisucan.model.User;

public interface UserDao {
    public User findByUserName(String userName);
    public boolean addUser(User user);
    public boolean updateUser(String password,int uid);
    public boolean isExist(String userName);
    
}
