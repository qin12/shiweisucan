package com.shiweisucan.daoimp;

import com.shiweisucan.dao.OrderDao1;
import com.shiweisucan.dao.ShopDao;
import com.shiweisucan.model.Order;
import com.shiweisucan.model.Shop;
import com.shiweisucan.util.DBHelper;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class OrderImpl1 implements OrderDao1{
    private QueryRunner runner;
    private Connection connection;

    public OrderImpl1() {
        // TODO Auto-generated constructor stub
        runner = new QueryRunner();
        connection = DBHelper.getConnection();
    }

    @Override
    public List<Order> showAllOrders() {
        String sql = "select * from orders";
        ResultSetHandler<List<Order>> rSetHandler = new BeanListHandler<Order>(Order.class);
        List<Order> orders = null;
        try {
            orders= runner.query(connection,sql, rSetHandler);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return orders;
    }

    @Override
    public Boolean addOrder(Order order) {
        Connection conn = null;
        PreparedStatement stmt = null;
        String sql = "insert into orders(id,name,xiangqing,img) "
                + "values(?,?,?,?)";

        try {
            conn = DBHelper.getConnection();
            stmt = conn.prepareStatement(sql);
            stmt.execute("set Names utf8");
            stmt.setInt(1, order.getId());
            stmt.setString(2,order.getName());
            stmt.setString(3,order.getXiangqing());
            stmt.setString(4,order.getImg());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                    stmt = null;
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                    conn = null;
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
