package com.shiweisucan.daoimp;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.DbUtils;
//import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import com.shiweisucan.dao.UserDao;
import com.shiweisucan.model.User;
import com.shiweisucan.util.DBHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserImpl implements UserDao {
    private QueryRunner runner;
    private Connection connection;

    public UserImpl() {
        // TODO Auto-generated constructor stu
        runner = new QueryRunner();
        connection = DBHelper.getConnection();
    }


    @Override
    public User findByUserName(String userName) {
        String sql = "select * from user where username = ?";
        ResultSetHandler<User> rsHandler = new BeanHandler<User>(User.class);
        User user = null;
        try {
            user = runner.query(connection, sql, rsHandler, userName);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return user;
    }

    @Override
    public boolean addUser(User user) {
        Connection conn = null;
        PreparedStatement stmt = null;
        String sql = "insert into user(userid,username,password) "
                + "values(?,?,?)";

        try {
            conn = DBHelper.getConnection();
            stmt = conn.prepareStatement(sql);
            stmt.execute("set Names utf8");
            stmt.setInt(1, user.getUserid());
            stmt.setString(2, user.getUsername());
            stmt.setString(3, user.getPassword());
            stmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                    stmt = null;
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                    conn = null;
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public boolean updateUser(String password, int uid) {
        String sql = "update user set password=? where uid=?";
        Object params[] = {password, uid};
        int a = 0;
        try {
            a = runner.update(connection,sql, params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (a > 0)
            return true;
        else
            return false;
    }


    @Override
    public boolean isExist(String userName) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String sql = "select * from user where username = ?";

        try {
            conn = DBHelper.getConnection();
            stmt = conn.prepareStatement(sql);
            stmt.execute("set Names utf8");
            stmt.setString(1, userName);
            rs = stmt.executeQuery();
            if (rs.next()) {
                System.out.println("此用户已存在");
                return true;
            } else {
                System.out.println("此用户不存在");
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                    rs = null;
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }

            if (stmt != null) {
                try {
                    stmt.close();
                    stmt = null;
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                    conn = null;
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
