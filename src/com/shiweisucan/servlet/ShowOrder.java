package com.shiweisucan.servlet;

import com.shiweisucan.daoimp.OrderImpl;
import com.shiweisucan.daoimp.UserImpl;
import com.shiweisucan.model.Order;
import com.shiweisucan.model.User;
import net.sf.json.JSONArray;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class ShowOrder extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json;charset=utf-8");//指定返回的格式为JSON格式
        response.setCharacterEncoding("UTF-8");//setContentType与setCharacterEncoding的顺序不能调换，否则还是无法解决中文乱码的问题
        String userName = request.getParameter("userName");
        System.out.println("查询order username=" + userName);
        List<Order> list;
        OrderImpl orderimp = new OrderImpl();
        UserImpl userimp = new UserImpl();
        User user ;
        user = userimp.findByUserName(userName);
        list = orderimp.findOrdersByUid(user.getUid());
        System.out.println(user.getUid());
        //创建json集合
        JSONArray jsonArray = JSONArray.fromObject(list);
        String jsonStr = jsonArray.toString();
        PrintWriter out =null;
        out =response.getWriter();
        out.write(jsonStr);
        out.close();
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
