package com.shiweisucan.servlet;

import com.shiweisucan.daoimp.OrderImpl1;
import com.shiweisucan.daoimp.UserImpl;
import com.shiweisucan.model.Order;
import com.shiweisucan.model.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class addOrder extends HttpServlet{
    public addOrder() {
        super();
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=utf-8");//指定返回的格式为JSON格式
        resp.setCharacterEncoding("UTF-8");//setContentType与setCharacterEncoding的顺序不能调换，否则还是无法解决中文乱码的问题

        String shopName = req.getParameter("shopName");
        String u = new String(shopName.getBytes("iso8859-1"),"utf-8");
        String xiangqing = req.getParameter("xiangqing");
        String v = new String(xiangqing.getBytes("iso8859-1"),"utf-8");
        String result = "0";
        Order order = new Order();
        order.setImg("https://www.qgxljm.cn/image/1.jpg");
        order.setName(u);
        order.setXiangqing(v);

            boolean flag1 = new OrderImpl1().addOrder(order);
            if(flag1) {
                System.out.println("添加用户成功");
                result = "1";
            }
            else result = "0";
        PrintWriter out =null;
        out =resp.getWriter();
        out.write(result);
        out.close();
    }
}
