package com.shiweisucan.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shiweisucan.daoimp.UserImpl;
import com.shiweisucan.model.User;

import java.io.IOException;
import java.io.PrintWriter;

public class Login extends HttpServlet {
    public Login() {
        super();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=utf-8");//指定返回的格式为JSON格式
        resp.setCharacterEncoding("UTF-8");//setContentType与setCharacterEncoding的顺序不能调换，否则还是无法解决中文乱码的问题
        String userName = req.getParameter("userName");
        String passWord = req.getParameter("passWord");
        String result = "0";
        UserImpl userimp = new UserImpl();
        //判断此用户是否存在
        boolean flag = userimp.isExist(userName);
        User user = null;
        if (!flag) {
            result = "0";
        } else {
            //判断此用户密码是否正确
            user = userimp.findByUserName(userName);
            int userid = user.getUserid();
            if (userid == 0) {
                result = "3";
            }

            String passWord1 = user.getPassword();
            if (passWord1.equals(passWord)) {
                result = "1";
            } else result = "2";
        }
        PrintWriter out = null;
        out = resp.getWriter();
        out.write(result);
        out.close();
    }

}
