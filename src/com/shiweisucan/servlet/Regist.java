package com.shiweisucan.servlet;

import com.shiweisucan.daoimp.UserImpl;
import com.shiweisucan.model.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class Regist extends HttpServlet{
    public Regist() {
        super();
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=utf-8");//指定返回的格式为JSON格式
        resp.setCharacterEncoding("UTF-8");//setContentType与setCharacterEncoding的顺序不能调换，否则还是无法解决中文乱码的问题

        String userName = req.getParameter("userName");
        String u = new String(userName.getBytes("iso8859-1"),"utf-8");
        String passWord = req.getParameter("passWord");
        String result = "0";

        UserImpl userimp = new UserImpl();
        //判断此用户是否存在
        boolean flag = userimp.isExist(u);
        if(flag) {
            result = "2";
        }
        else {
            User user = new User();
            user.setUserid(1);
            user.setUsername(u);
            user.setPassword(passWord);
            boolean flag1 = userimp.addUser(user);
            if(flag1) {
                System.out.println("添加用户成功");
                result = "1";
            }
            else result = "0";
        }
        PrintWriter out =null;
        out =resp.getWriter();
        out.write(result);
        out.close();
    }
}
