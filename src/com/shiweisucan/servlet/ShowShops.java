package com.shiweisucan.servlet;

import com.shiweisucan.daoimp.ShopImpl;
import com.shiweisucan.model.Shop;
import net.sf.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class ShowShops extends HttpServlet {
    public ShowShops() {
        super();
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json;charset=utf-8");//指定返回的格式为JSON格式
        response.setCharacterEncoding("UTF-8");//setContentType与setCharacterEncoding的顺序不能调换，否则还是无法解决中文乱码的问题
        ShopImpl shopimpl = new ShopImpl();
        List<Shop> list = shopimpl.showAllShops();
        if(list != null) {
            System.out.println("获取shops列表成功");
        }
        System.out.println(list.get(0).getName());
        //创建json集合
        JSONArray jsonArray = JSONArray.fromObject(list);
        String jsonStr = jsonArray.toString();
        PrintWriter out =null;
        out =response.getWriter();
        out.write(jsonStr);
        out.close();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
